using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Wander
{
    struct TileDownload
    {
        public int deltaTileX;
        public int deltaTileY;
        public int absTileX;
        public int absTileY;
        public UnityWebRequest request;
    }

    [ExecuteInEditMode()]
    public class DownloadTopo : MonoBehaviour
    {
        public enum MapType
        {
            Trees,
            BGT_achtergrond,
            BGT_standaard,
            BGT_planvisualisatie,
            BGT_omtrekgerichtevisualisatie,
            BGT_pastelvisualisatie,
            BGT_icoonvisualisatie,
            BRT
        }

#if UNITY_EDITOR
        public MapType mapType;
        public bool runOnStart;
        public bool doIt;
        public bool cancel;
        public float offsetX = 174041;
        public float offsetY = 444126;
        public int zoom = 11;
        public int numTilesWidth  = 10;
        public int numTilesHeight = 10;
        public GameObject tilePrefab;

        GameObject root;
        int numRetries = 0;
        bool showedResult = false;
        List<TileDownload> requests = new List<TileDownload>();


        private void Start()
        {
            doIt = false;
            if (Application.isPlaying)
            {
                if (runOnStart)
                    doIt = true;
            }
        }

        void Update()
        {
            if ( cancel )
            {
                cancel = false;
                numRetries = 0;
                showedResult = false;
                requests.Clear();
            }

            if ( doIt )
            {
                doIt = false;
                numRetries = 0;
                showedResult = false;
                requests.Clear();
                Download();
            }

            CheckDownloadsDone();
        }

        string GetUrl(int tileX, int tileY, int zoom)
        {
            string url = "";
            switch( mapType )
            {
                case MapType.BGT_achtergrond:
                    url = $"https://service.pdok.nl/lv/bgt/wmts/v1_0/achtergrondvisualisatie/EPSG:28992/{zoom}/{tileX}/{tileY}.png";
                    break;
                case MapType.BGT_standaard:
                    url = $"https://service.pdok.nl/lv/bgt/wmts/v1_0/standaardvisualisatie/EPSG:28992/{zoom}/{tileX}/{tileY}.png";
                    break;
                case MapType.BGT_planvisualisatie:
                    url = $"https://service.pdok.nl/lv/bgt/wmts/v1_0/planvisualisatie/EPSG:28992/{zoom}/{tileX}/{tileY}.png";
                    break;
                case MapType.BGT_icoonvisualisatie:
                    url = $"https://service.pdok.nl/lv/bgt/wmts/v1_0/icoonvisualisatie/EPSG:28992/{zoom}/{tileX}/{tileY}.png";
                    break;
                case MapType.BGT_omtrekgerichtevisualisatie:
                    url = $"https://service.pdok.nl/lv/bgt/wmts/v1_0/omtrekgerichtevisualisatie/EPSG:28992/{zoom}/{tileX}/{tileY}.png";
                    break;
                case MapType.BGT_pastelvisualisatie:
                    url = $"https://service.pdok.nl/lv/bgt/wmts/v1_0/pastelvisualisatie/EPSG:28992/{zoom}/{tileX}/{tileY}.png";
                    break;
                case MapType.BRT:
                    url = $"https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0?layer=standaard&style=default&tilematrixset=EPSG:28992&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/Fpng&TileMatrix=EPSG:28992:{zoom}&TileCol={tileX}&TileRow={tileY}";
                    break;
                case MapType.Trees:
                    url = $"https://services.geodan.nl/data/geodan/gws/nld/landuse/wmts?layer=trees&style=default&tilematrixset=EPSG:28992&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/png&TileMatrix={zoom}&TileCol={tileX}&TileRow={tileY}&servicekey=b28cce8a-abe7-11e6-84a1-005056805b87";
                    break;
            }
            return url;
        }

        GameObject Download()
        {
            Debug.Log( "Start downloading Topo" );
            Vector2Int tileCentre = RDUtils.RD2Tile( new Vector2( offsetX, offsetY ), zoom );

            // Calculate how much the chosen centre RD is from tile alignment.
            Vector2 rdTileCentre = RDUtils.Tile2RD( tileCentre, zoom );
            float rdShiftX  = offsetX - rdTileCentre.x;
            float rdShiftY  = offsetY - rdTileCentre.y;
            // Apply the shift to the root.
            root = new GameObject(mapType.ToString());
            root.transform.position = -new Vector3( rdShiftX, 0, rdShiftY );

            // Start downloading all the tiles
            for ( int x = -numTilesWidth/2; x < numTilesWidth/2; x++ )
            {
                for ( int y = -numTilesHeight/2; y < numTilesHeight/2; y++ )
                {
                    int tileX = x + tileCentre.x;
                    int tileY = y + tileCentre.y;
                    string url = GetUrl(tileX, tileY, zoom);
                    UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
                    www.SendWebRequest();
                    TileDownload td = new TileDownload();
                    td.request = www;
                    td.deltaTileX = x;
                    td.deltaTileY = -y;
                    td.absTileX = tileX;
                    td.absTileY = tileY;
                    requests.Add( td );
                }
            }

            return root;
        }

        void CheckDownloadsDone()
        {
            if (root==null)
                return;
            float tileSize = (float) RDUtils.CalcTileSizeRD(zoom);
            for ( int i = 0; i < requests.Count; i++ )
            {
                var td  = requests[i];
                var www = td.request;
                if ( www.isDone )
                {
                    if ( www.result != UnityWebRequest.Result.Success )
                    {
                        Debug.Log( www.error );
                        // retry
                        td.request = UnityWebRequestTexture.GetTexture( www.url );
                        td.request.SendWebRequest();
                        requests[i] = td;
                        numRetries++;
                    }
                    else
                    {
                        GameObject tile = Instantiate(tilePrefab);
                        // setup transform
                        tile.transform.parent = root.transform;
                        tile.transform.localPosition = new Vector3( td.deltaTileX * tileSize, 0, td.deltaTileY * tileSize );
                        tile.transform.localScale = new Vector3( tileSize, 1, tileSize );
                        // setup material
                        MeshRenderer mr = tile.transform.GetChild( 0 ).GetComponent<MeshRenderer>();
                        Material mat    = new Material(mr.sharedMaterial);
                        mat.mainTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                        mr.sharedMaterial = mat;
                        tile.name = $"Tile_{td.deltaTileX}_{td.deltaTileY}_{td.absTileX}_{td.absTileY}";
                        // remove request
                        requests.RemoveAt( i );
                        i--;
                        continue;
                    }
                }
            }

            if ( requests.Count == 0 && !showedResult )
            {
                Debug.Log( "Finished downloading Topo. Num retires: " + numRetries );
                showedResult = true;
            }
        }
#endif
    }
}