## BGT Topo NL (HDRP)

Retrieves card information from BGT or other cards using TMS
service. Aligns with buildings and terrain package.

## Dependencies
- Utils

### Usage:

Sweep the TopoDownload prefab into the Hierarchy and press 'doIt'.

